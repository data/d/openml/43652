# OpenML dataset: SDSS-16

https://www.openml.org/d/43652

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data is pulled from SDSS Skyserver from data release 16 using the following query.

SELECT
  p.objid,p.ra,p.dec,p.u,p.g,p.r,p.i,p.z, p.run, p.rerun, p.camcol, p.field,
  s.specobjid, s.class, s.z as redshift, s.plate, s.mjd, s.fiberid
INTO mydb.MyTable_0
FROM PhotoObj AS p
JOIN SpecObj AS s ON s.bestobjid = p.objid
WHERE p.u BETWEEN 0 AND 19.6 AND g BETWEEN 0 AND 20

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43652) of an [OpenML dataset](https://www.openml.org/d/43652). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43652/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43652/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43652/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

